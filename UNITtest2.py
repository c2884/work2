
def test_encode_ham():
    assert encode_ham('S1', 'R1', 'hi') == '.... .. = .-. .---- = -.. . ... = .---- = -...- .... .. = -.-- . = -.--.-'

def test_decode_ham():
    assert decode_ham('.... .. = .-. .---- = -.. . ... = .---- = -...- .... .. = -.-- . = -.--.-') == ('S1', 'R1', 'hi')

def test_both_functions():
    message = 'Hello, how are you?'
    encoded_message = encode_ham('Alice', 'Bob', message)
    decoded_message = decode_ham(encoded_message)
    assert decoded_message == ('Alice', 'Bob', message)
