import websocket

def send_echo(sender: str, msg: str) -> str:
    # Encode the message as extended Morse code
    encoded_msg = encode_ham(sender, 'echo', msg)
    
    # Create a WebSocket connection to the server
    ws = websocket.create_connection("ws://localhost:10102")
    
    # Send the message to the server
    ws.send(encoded_msg)
    
    # Wait for the response from the server
    response = ws.recv()
    
    # Decode the response from extended Morse code
    decoded_response = decode_ham(response)[2]
    
    # Close the WebSocket connection
    ws.close()
    
    return decoded_response

The `send_time` function is similar, but the message sent to the server is different:

def send_time(sender: str) -> str:
    # Encode the message as extended Morse code
    encoded_msg = encode_ham(sender, 'time', 'hello world')
    
    # Create a WebSocket connection to the server
    ws = websocket.create_connection("ws://localhost:10102")
    
    # Send the message to the server
    ws.send(encoded_msg)
    
    # Wait for the response from the server
    response = ws.recv()
    
    # Decode the response from extended Morse code
    decoded_response = decode_ham(response)[2]
    
    # Close the WebSocket connection
    ws.close()
    
    return decoded_response
