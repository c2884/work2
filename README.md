
# Worksheet 2

A brief description of what this project does and who it's for

This repository contains three Python programs for working with Morse code. The programs use the morse dictionary to translate letters and numbers to and from Morse code.

Prerequisites
Python 3.x
websocket library
decode_bt
decode_bt is a function that takes a Morse code message as a string and returns the corresponding text message. It uses a binary heap to efficiently search for the Morse code patterns in the message.

To use decode_bt, import the function and call it with a Morse code message as a string. Example:

from morse_code import decode_bt

msg = "--. ..- -.--. .... --- -. --..-- .-.-.- -.--.-"
decoded_msg = decode_bt(msg)

print(decoded_msg)  # "GUCHONG,."


dijkstra
dijkstra is a function that implements Dijkstra's algorithm to find the shortest path between two nodes in a weighted graph. It uses the heapq module to efficiently store and update the distances of the nodes.

To use dijkstra, define a weighted graph as a dictionary of dictionaries, where the keys are the nodes and the values are dictionaries containing the neighbors and their weights. Then import the function and call it with the graph, start node, and end node. Example:

from morse_code import dijkstra

graph = {
    'A': {'B': 5, 'C': 1},
    'B': {'A': 5, 'C': 2, 'D': 1},
    'C': {'A': 1, 'B': 2, 'D': 4},
    'D': {'B': 1, 'C': 4}
}

start = 'A'
end = 'D'

shortest_dist = dijkstra(graph, start, end)

print(shortest_dist)  # 3


send_echo and send_time
send_echo and send_time are functions that send messages to a WebSocket server and receive responses. They use the websocket library to create a WebSocket connection, encode the message as extended Morse code using the encode_ham function, send the message to the server, receive the response, decode the response from extended Morse code using the decode_ham function, and close the WebSocket connection.

To use send_echo or send_time, import the function and call it with the sender's name and message (for send_echo) or just the sender's name (for send_time). Example:

from morse_code import send_echo, send_time

sender = "Alice"
msg = "Hello world"

echo_response = send_echo(sender, msg)
time_response = send_time(sender)

print(echo_response)  # "HELLO WORLD"
print(time_response)  # "2023-02-19 14:30:00"
