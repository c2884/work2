from heapq import heappush, heappop

def decode_bt(msg: str) -> str:
    morse = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 
             'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 
             'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 
             'Y': '-.--', 'Z': '--..', '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-', 
             '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.'}
    
    # Convert message to a list of characters
    msg_list = list(msg)
    
    # Remove spaces from the message list
    msg_list = [char for char in msg_list if char != ' ']
    
    # Create a binary heap of the Morse code dictionary, with the length of each code as the priority
    heap = [(len(code), code) for code in morse.values()]
    heapify(heap)
    
    # Initialize variables for decoding the message
    result = ''
    node = ''
    
    while msg_list:
        # Add the next character to the current node
        node += msg_list.pop(0)
        
        # If the current node is in the binary heap, add the corresponding letter to the result and reset the current node
        if any(node == code for _, code in heap):
            letter = list(morse.keys())[list(morse.values()).index(node)]
            result += letter
            node = ''
        
        # If the current node is not in the binary heap, continue to the next character
        
    return result
