def test_decode_bt():
    assert decode_bt('.... . .-.. .-.. --- / .-- --- .-. .-.. -.. /') == 'HELLO WORLD'
    assert decode_bt('--. ..- -.-- / -.-- --- ..- / ..- .--. .-.-.-') == 'GUY YOU UP.'
    assert decode_bt('... --- ... / .- ..- - / - .... .. ... /') == 'SOS AUSTHIS'
