import heapq

def dijkstra(graph, start, end):
    # Initialize distances to all nodes as infinite except for the start node
    dist = {node: float('inf') for node in graph}
    dist[start] = 0

    # Create a priority queue and add the start node to it
    pq = [(0, start)]

    while pq:
        # Pop the node with the minimum distance from the start node
        (cur_dist, cur_node) = heapq.heappop(pq)

        # If the popped node is the destination node, return its distance
        if cur_node == end:
            return cur_dist

        # Otherwise, visit all neighbors of the current node and update their distances
        for neighbor, weight in graph[cur_node].items():
            distance = cur_dist + weight
            if distance < dist[neighbor]:
                dist[neighbor] = distance
                heapq.heappush(pq, (distance, neighbor))

    # If destination node is not reached, return infinity
    return float('inf')
