def test_send_echo():
    assert send_echo('Alice', 'Hello, echo server!') == 'Hello, echo server!'

def test_send_time():
    assert len(send_time('Alice')) > 0

#Note that the `send_time` function does not return a specific message, but rather the current time from the server. Therefore, we simply check that the length of the returned string is greater than zero.

#With these functions implemented and tested, we can now communicate with the extended Morse server over a WebSocket connection.
